package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new Quack());
    }

    @Override
    public void display() {
        System.out.println("Looks like a Model Duck");
    }
}
